"use strict";
const fetch = require("node-fetch");
const DomParser = require("dom-parser");
const parser = new DomParser();

const url = "https://codequiz.azurewebsites.net/";
const args = process.argv.slice(2);

const printNavValue = (row) => {
  console.log(row.getElementsByTagName("td")[1].textContent);
};

fetch(url, {
  method: "GET",
  headers: {
    Cookie: "hasCookie=true",
  },
})
  .then((res) => res.text())
  .then((html) => {
    const dom = parser.parseFromString(html);
    const table = dom.getElementsByTagName("table")[0];
    const rows = table.getElementsByTagName("tr");
    let rowIndex;
    switch (args[0]) {
      case "B-INCOMESSF":
        rowIndex = 1;
        break;
      case "BM70SSF":
        rowIndex = 2;
        break;
      case "BEQSSF":
        rowIndex = 3;
        break;
      case "B-FUTURESSF":
        rowIndex = 4;
        break;
      default:
        rowIndex = 0;
    }
    if (rowIndex > 0 && rowIndex <= 4) {
      printNavValue(rows[rowIndex]);
    } else {
      console.log(
        "pls input first argument B-INCOMESSF | BM70SSF | BEQSSF | B-FUTURESSF "
      );
    }
  });
